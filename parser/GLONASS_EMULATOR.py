import xlrd, xlwt
import json
import urllib
from urllib import request, parse
import json

rb = xlrd.open_workbook('Q00000000000000D_1.xlsx')

sheet = rb.sheet_by_index(0)

l = []
myurl = "http://127.0.0.1:3000/addCarData"

for i in range(70, 170):
# {'lat': 557.757, 'lng': 374.42, 'rotation': 228.0, 'time': '2018-08-01 08:24:41 +0300 MSK', 'event': 'DISTANCE',
# 'vin': 'X9600000000000430', 'odometer': 58.0, 'airbag_fired': 'false', 'power_status': 'true',
# 'remaining_mileage': 12680.0, 'fuel_level': 58.0, 'coolant_level_low': 'false', 'generator_malfunction': 'false',
# 'break_fluid_low_level': 'false', 'coolant_temp': 80.0}
    time = sheet.row_values(i)[1]
    lat = round(float(sheet.row_values(i)[8]/3600/1000), 5)
    lng = round(float(sheet.row_values(i)[7]/3600/1000), 5)
    event_type = sheet.row_values(i)[4]
    vin = sheet.row_values(i)[5]
    odometer = sheet.row_values(i)[26]
    airbag_fired = sheet.row_values(i)[21]
    power_status = sheet.row_values(i)[20]
    remaining_mileage = sheet.row_values(i)[25]
    fuel_level = sheet.row_values(i)[26]
    coolant_level_low = sheet.row_values(i)[31]
    generator_malfunction = sheet.row_values(i)[32]
    break_fluid_low_level = sheet.row_values(i)[33]
    coolant_temp = sheet.row_values(i)[28]
    course = sheet.row_values(i)[10]
    max_rpm = sheet.row_values(i)[34]
    engine_oil_pressure = sheet.row_values(i)[29]
    message = {'lat': lat,
               'lng': lng,
               'course': course,
               'time': time,
               'event_type': event_type,
               'vin': vin,
               'odometer': odometer,
               'airbag_fired': airbag_fired,
               'power_status':power_status,
               'remaining_mileage':remaining_mileage,
               'fuel_level': fuel_level,
               'coolant_level_low': coolant_level_low,
               'generator_malfunction': generator_malfunction,
               'break_fluid_low_level': break_fluid_low_level,
               'coolant_temp': coolant_temp,
               'max_rpm': max_rpm,
               'engine_oil_pressure': engine_oil_pressure
               }
    # Post Method is invoked if data != None10
    print(message)
    req = urllib.request.Request(myurl)
    req.add_header('Content-Type', 'application/json; charset=utf-8')
    jsondata = json.dumps(message)
    jsondataasbytes = jsondata.encode('utf-8')   # needs to be bytes
    req.add_header('Content-Length', len(jsondataasbytes))
    urllib.request.urlopen(req, jsondataasbytes)